<?php

namespace App\Service;

use App\Exception\InsertInDatabaseInvalidException;
use App\Entity\DeviceOxygen;
use App\Repository\DeviceOxygenRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ImportDevicesOxygenService
{
    private EntityManagerInterface $em;
    private DeviceOxygenRepository $oxygenRepository;
    private ManufacturerService $manufacturerService;
    private OrangeSegmentationService $orangeSegmentationService;

    public function __construct(EntityManagerInterface $em, DeviceOxygenRepository $repositoryOxygen, ManufacturerService $manufacturerService, OrangeSegmentationService $orangeSegmentationService)
    {
        $this->em = $em;
        $this->oxygenRepository = $repositoryOxygen;
        $this->manufacturerService = $manufacturerService;
        $this->orangeSegmentationService = $orangeSegmentationService;
    }

    /**
     * @param string $fileName
     * @return string the entire file to be read
     * Get datas from file
     */
    public function getDatasOxygen(string $fileName): string
    {
        return file_get_contents($fileName);
    }

    /**
     * @param string $datas
     * @return array of counter Updated, Created Devices and Manufacturers
     * Main method being call to import datas in db
     */
    public function importOxygen(string $datas): array
    {
        ini_set('memory_limit', '512M');
        $devices = $this->getCsvRowsAsArrays($datas);
        $i = 0;
        $packageSize = 100;
        $updatedDeviceItemCount = 0;
        $newDeviceItemCount = 0;
        $existingDeviceByNacode = $this->deviceOxygenByNaCode();
        $listManufacturer = [];
        $listSegmentation = [];

        foreach ($devices as $device) {
            if (isset($existingDeviceByNacode[$device['NA Code']])) {
                $res = $this->setDeviceItem($device, $existingDeviceByNacode[$device['NA Code']]);
                if ($res) {
                    $updatedDeviceItemCount++;
                }
            } else {
                $this->setDeviceItem($device);
                $newDeviceItemCount++;
            }

            if ($device['Manufacturer Name'] !== ''){
                $listManufacturer[$device['Manufacturer Name']] = $device['Manufacturer Name'];
            }

            if (isset($device['Orange segmentation']) && $device['Orange segmentation'] !== ''){
                $listSegmentation[$device['Orange segmentation']] = $device['Orange segmentation'];
            }
            // Split the the flush of datas by packages of 100 (too many datas to be flush at once)
           if (($i % $packageSize) === 0) {
                try {
                    $this->em->flush();
                }catch(Exception $e){
                    throw new InsertInDatabaseInvalidException();
                }
            }
            $i++;
        }

        try {
            $this->em->flush();
        } catch(Exception $e){
            throw new InsertInDatabaseInvalidException();
        }

        $nbManufacturer = $this->manufacturerService->setManufacturer(array_unique($listManufacturer), 'oxygen');
        $nbSegmentation = $this->orangeSegmentationService->setOrangeSegmentation($listSegmentation);

        return [
            'updatedDeviceItemCount' => $updatedDeviceItemCount,
            'newDeviceItemCount' => $newDeviceItemCount,
            'newManufacturerCount' => $nbManufacturer,
            'newOrangeSegmentationCount' => $nbSegmentation
        ];
    }

    /**
     * @param array $device
     * @param DeviceOxygen|null $existingDeviceItem
     * @return bool true if device updated
     * Create or Update device if exist
     */
    private function setDeviceItem(array $device, DeviceOxygen $existingDeviceItem = null)
    {
        if ($existingDeviceItem === null){
            $deviceItem =  new DeviceOxygen();
            $deviceItem->setNaCode($device['NA Code']);
        } else {
            $deviceItem = $existingDeviceItem;
        }
        // to be able to detect the changes
        $cloneDevice = clone $deviceItem;

        !isset($device['Advanced receiver']) ?: $deviceItem->setAdvancedReceiver(htmlentities($this->isDataStrTooLong($device['Advanced receiver'])));
        !isset($device['Orange segmentation']) ?: $deviceItem->setOrangeSegmentation(htmlentities($device['Orange segmentation']));
        !isset($device['Manufacturer Name']) ?: $deviceItem->setManufacturerName(htmlentities($device['Manufacturer Name']));
        !isset($device['Product Type*']) ?: $deviceItem->setProductType(htmlentities($device['Product Type*']));
        !isset($device['Manufacturer Product Name']) ?: $deviceItem->setManufacturerProductName(htmlentities($this->isDataStrTooLong($device['Manufacturer Product Name'])));
        !isset($device['Commercial Name']) ?: $deviceItem ->setCommercialName(htmlentities($this->isDataStrTooLong($device['Commercial Name'])));
        !isset($device['Resolution density (PPI)']) ?: $deviceItem->setResolutionDensity($this->transformStringToNull($device['Resolution density (PPI)']));
        !isset($device['Height (pixel)']) ?: $deviceItem->setHeightPixel($this->transformStringToNull($device['Height (pixel)']));
        !isset($device['Screen size (in inches) (diagonal)']) ?: $deviceItem->setScreenSize($this->transformStringToNull($device['Screen size (in inches) (diagonal)']));
        !isset($device['Display technology']) ?: $deviceItem->setDisplayTechnology(htmlentities($this->isDataStrTooLong($device['Display technology'])));
        !isset($device['Width (pixel)']) ?: $deviceItem->setWidth($this->transformStringToNull($device['Width (pixel)']));
        !isset($device['Brightness (cd/m²)']) ?: $deviceItem->setBrightness($this->transformStringToNull($device['Brightness (cd/m²)']));
        !isset($device['LTE voice support']) ?: $deviceItem->setLteVoiceSupport(explode('|',$device['LTE voice support']));
        !isset($device['LTE Category']) ?: $deviceItem->setLteCategory(htmlentities($this->isDataStrTooLong($device['LTE Category'])));
        !isset($device['LTE mode']) ?: $deviceItem->setLteMode(htmlentities($this->isDataStrTooLong($device['LTE mode'])));
        !isset($device['LTE']) ?: $deviceItem->setTechno4g(htmlentities($this->transformStringToBoolean($device['LTE'])));
        !isset($device['2G']) ?: $deviceItem ->setTechno2g(htmlentities($this->transformStringToBoolean($device['2G'])));
        !isset($device['GPRS']) ?: $deviceItem ->setGprs(htmlentities($this->transformStringToBoolean($device['GPRS'])));
        !isset($device['HSDPA']) ?: $deviceItem ->setCategoryHsdpa(htmlentities($this->isDataStrTooLong($device['HSDPA'])));
        !isset($device['HSUPA']) ?: $deviceItem ->setCategoryHsupa(htmlentities($this->isDataStrTooLong($device['HSUPA'])));
        !isset($device['3G']) ?: $deviceItem->setTechno3g(htmlentities($this->transformStringToBoolean($device['3G'])));
        !isset($device['VoLTE']) ?: $deviceItem->setVolte(htmlentities($this->transformStringToBoolean($device['VoLTE'])));
        !isset($device['ViLTE']) ?: $deviceItem->setVilte(htmlentities($this->transformStringToBoolean($device['ViLTE'])));
        !isset($device['LTE MIMO DownLink']) ?: $deviceItem->setLteMimoDownlink(htmlentities($this->isDataStrTooLong($device['LTE MIMO DownLink'])));
        !isset($device['LTE MIMO UpLink']) ?: $deviceItem->setLteMimoDownlink(htmlentities($this->isDataStrTooLong($device['LTE MIMO UpLink'])));
        !isset($device['Dual Carrier']) ?: $deviceItem->setDualCarrier(htmlentities($this->transformStringToBoolean($device['Dual Carrier'])));
        !isset($device['Maximum number of aggregated carriers']) ?: $deviceItem->setMaximumNumberOfAggregatedCarriers(htmlentities($this->transformStringToNull($device['Maximum number of aggregated carriers'])));
        !isset($device['LTE Bands']) ?: $deviceItem->setLteBands(explode('|',$device['LTE Bands']));
        !isset($device['GSM Bands']) ?: $deviceItem->setGsmBands(explode('|',$device['GSM Bands']));
        !isset($device['UMTS Bands']) ?: $deviceItem->setUmtsBands(explode('|',$device['UMTS Bands']));
        !isset($device['SIM card format']) ?: $deviceItem->setSimcardFormat(htmlentities($this->isDataStrTooLong($device['SIM card format'])));
        !isset($device['Multiple SIM Support']) ?: $deviceItem->setMultipleSimSupport($this->transformStringToNull($device['Multiple SIM Support']));
        !isset($device['Battery capacity (mAh)']) ?: $deviceItem->setBatteryCapacity($this->transformStringToNull($device['Battery capacity (mAh)']));
        !isset($device['Battery Type']) ?: $deviceItem->setBatteryType(htmlentities($this->isDataStrTooLong($device['Battery Type'])));
        !isset($device['Fast Charging']) ?: $deviceItem->setFastCharging(htmlentities($this->isDataStrTooLong($device['Fast Charging'])));
        !isset($device['Removable']) ?: $deviceItem->setRemovable(htmlentities($this->transformStringToBoolean($device['Removable'])));
        !isset($device['Wireless charging']) ?: $deviceItem->setWirelessCharging(htmlentities($this->transformStringToBoolean($device['Wireless charging'])));
        !isset($device['LTE Category DownLink']) ?: $deviceItem->setLteCategoryDownlink(htmlentities($this->isDataStrTooLong($device['LTE Category DownLink'])));
        !isset($device['LTE Category UpLink']) ?: $deviceItem->setLteCategoryUplink(htmlentities($this->isDataStrTooLong($device['LTE Category UpLink'])));
        !isset($device['EDGE']) ?: $deviceItem->setEdge($this->transformStringToBoolean(($device['EDGE'])));
        !isset($device['Wi-Fi']) ?: $deviceItem->setWiFi(explode("|",$device['Wi-Fi']));
        !isset($device['Wi-Fi protected setup (Easy Pairing)']) ?: $deviceItem->setWiFiProtectedSetup(htmlentities($this->transformStringToBoolean($device['Wi-Fi protected setup (Easy Pairing)'])));
        !isset($device['WPS Profile']) ?: $deviceItem->setWpsProfile(htmlentities($this->isDataStrTooLong($device['WPS Profile'])));
        !isset($device['Baseband Chipset name']) ?: $deviceItem->setBaseBandChipsetName(htmlentities($this->isDataStrTooLong($device['Baseband Chipset name'])));
        !isset($device['Baseband chipset supplier']) ?: $deviceItem->setBasebandChipsetSupplier(htmlentities($device['Baseband chipset supplier']));
        !isset($device['VoWI-FI']) ?: $deviceItem->setVoWiFi(htmlentities($this->transformStringToBoolean($device['VoWI-FI'])));
        !isset($device['Operating System']) ?: $deviceItem->setOperatingSystem(htmlentities($this->isDataStrTooLong($device['Operating System'])));
        !isset($device['Operating System Version']) ?: $deviceItem->setOperatingSystem(htmlentities($device['Operating System Version']));
        !isset($device['Creation date']) ?: $deviceItem->setCreatedAt(DateTime::createFromFormat('Y/m/d H:i:s', explode(' ', $device['Creation date'])[0]." 00:00:00"));
        !isset($device['Last update date']) ?: $deviceItem->setUpdatedAt(DateTime::createFromFormat('Y/m/d H:i:s', explode(' ', $device['Last update date'])[0]." 00:00:00"));
        !isset($device['5G']) ?: $deviceItem->setTechno5g(htmlentities($this->isDataStrTooLong($device['5G'])));
        !isset($device['5G Sul Support']) ?: $deviceItem->setTechno5gSulSupport(htmlentities($this->transformStringToBoolean($this->isDataStrTooLong($device['5G Sul Support']))));
        !isset($device['MIMO configuration']) ?: $deviceItem->setMimoConfiguration(htmlentities($this->isDataStrTooLong($device['MIMO configuration'])));

        $this->em->persist($deviceItem);

        return !($deviceItem == $cloneDevice); // return true if changes beetween 2 objects, we use "==" and not  "===" because strict equality will always return true in that case
    }

    /**
     * @param string $datas
     * @return array
     * Serialize CSV datas as an Array of key => value
     */
    public function getCsvRowsAsArrays(string $datas) : array
    {
        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);
        return $serializer->decode($datas, 'csv', [CsvEncoder::DELIMITER_KEY => "\t", CsvEncoder::ENCLOSURE_KEY => '"']);
    }

    /**
     * @param $data
     * @return bool|string
     * Transform $data different of YES and NO in false
     */
    private function transformStringToBoolean($data): bool|string
    {
        return ($data !== 'YES' && $data !== 'NO') ? false : $data;
    }

    /**
     * @param $data
     * @return int|null
     * Transform data !numeric in null
     */
    public function transformStringToNull($data): ?int
    {
        return (!is_numeric($data)) ? null : $data;
    }

    /**
     * @param $str
     * @return mixed|string
     * Handle the massive lines of string to avoid sql errors
     */
    private function isDataStrTooLong($str)
    {
        if (strlen($str) >= 255){
            return "Data was too long, couldn't import the value";
        } else {
            return $str;
        }
    }

    /**
     * @return array
     * Find existing devices in db by NaCode
     */
    private function deviceOxygenByNaCode(): array
    {
        $results = $this->oxygenRepository->findAll();
        $resByNacode = [];
        foreach ($results as  $device) {
            $resByNacode[$device->getNaCode()] = $device;
        }
        return $resByNacode;
    }
}