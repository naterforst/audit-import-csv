<?php

namespace App\Controller\UploadFile;

use App\Service\ImportDevicesOxygenService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class UploadOxygenFile extends AbstractController
{
    private ImportDevicesOxygenService $importDevicesOxygenService;

    public function __construct(ImportDevicesOxygenService $importDevicesOxygenService)
    {
        $this->importDevicesOxygenService = $importDevicesOxygenService;
    }

    #[Route('/api/upload_oxygen', methods: ['POST'])]
    public function uploadOxygenFile(Request $request): Response
    {
        /* @var $uploadedFile UploadedFile */
        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }

        $originalFileName = $uploadedFile->getClientOriginalName();

        if(!preg_match('/^.*\.(csv|CSV)$/', $originalFileName) && $uploadedFile->getClientMimeType() !== 'text/csv'){
            throw new BadRequestException('"file" is not a csv mime type');
        }

        $responseArrayDevicesUpdated = $this->importDevicesOxygenService->importOxygen($this->importDevicesOxygenService->getDatasOxygen($uploadedFile->getPathname()));

        return $this->json($responseArrayDevicesUpdated);
    }
}