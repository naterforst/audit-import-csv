<?php

namespace App\Command;

use App\Service\ImportDevicesOxygenService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:import-oxygen-devices',
    description: 'Import des données Oxygen',
)]
class ImportOxygenDeviceCommand extends Command{

    private ImportDevicesOxygenService $importDevicesOxygen;

    public function __construct(ImportDevicesOxygenService $importDevicesOxygen)
    {
        parent::__construct();
        $this->importDevicesOxygen = $importDevicesOxygen;
    }

    protected function configure()
    {
        parent::configure();
        $this->addOption('path-file','f',InputOption::VALUE_REQUIRED,'Give the path of the file');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $devicesArrayCount = $this->importDevicesOxygen->importOxygen($this->importDevicesOxygen->getDatasOxygen($input->getOption('path-file')));

        $io = new SymfonyStyle($input,$output);
        $io->success($devicesArrayCount['updatedDeviceItemCount'] . " device(s) Oxygen mis à jour.");
        $io->success($devicesArrayCount['newDeviceItemCount'] .  " nouveau(x) device(s) Oxygen ajouté(s)");
        $io->success($devicesArrayCount['newManufacturerCount'] . " manufacturer(s) ajouté(s)");
        $io->success($devicesArrayCount['newOrangeSegmentationCount'] . " ségmentation Orange(s) ajoutée(s)");

        return Command::SUCCESS;
    }
}