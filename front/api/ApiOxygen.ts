import { ApiResource } from "./ApiResource";
import { ISelectListType } from "../src/types/IGlobalType";
import { IDeviceOxygenType } from "../src/types/IDeviceOxygenType";

class ApiOxygen extends ApiResource {
  static baseUrl = "/device_oxygens";

  static async find(search: string): Promise<IDeviceOxygenType[]> {
    const response = ApiResource.axios.get(this.baseUrl + "?naCode=" + search);
    return await ApiResource.handleResponseCollection(response);
  }

  static async getFieldValues(nameField: string): Promise<ISelectListType[]> {
    const response = ApiResource.axios.get(
      "device-oxygens/fields-values?field=" + nameField
    );
    return await ApiResource.handleResponseItem(response); //ResponseItem cause it's not a jsonLd format
  }
}

export default ApiOxygen;
