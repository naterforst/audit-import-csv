import { ApiResource } from "./ApiResource";

class ApiUploadOxygen extends ApiResource {
  static baseUrl = "/upload_oxygen";

  static async post(param: any): Promise<any> {
    const response = ApiResource.axios.post(this.baseUrl, param, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
    return await ApiResource.handleResponseItem(response);
  }
}

export default ApiUploadOxygen;
