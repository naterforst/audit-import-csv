# audit-import-csv

Selection d'une fonctionnalitée sur laquelle j'ai travaillé dans le but de faire un audit de code

## La fonctionnailité d'import

Cet import a été réalisé sur un projet professionnel, le but était simplement de pouvoir permettre aux utilisateurs d'importer des données de devices dans la bdd de l'application via un fichier csv fourni. 

La partie back est développée sous Symfony avec API Platform.
- L'import peut être exécuté via une commande qui appelle simplement le service d'import 
- Un controller permet d'upload le fichier sur le serveur/ dans le projet temporairement. Le controller est appelé via une requête post vers /api/upload_oxygen avec un fichier joint
- Des tests fonctionnels d'api basiques ont était mis en place pour vérifier le bon fonctionnement des requêtes
- Le service traite les données, les converties sous forme d'un tableau pour ensuite les importer en base via l'ORM. Il y a une gestion d'erreur basique avec une Exception custom et des compteurs d'ajouts et modifications.

La partie front est développé sous Vue JS, Vuetify et TS
- Un composant formulaire d'Upload
- Gestion des erreurs avec une snackbar error & warning
- Informations sur le nombre de devices ajouté et modifié dans une snackbar success
- Un fichier Api pour la requete post custom d'upload et un fichier Api pour trouver un sample par nacode et trouver les valeurs d'un champ spécifique

--> Aperçu 
![image.png](./image.png)

# audit

- [ ] Clarté du code 
- [ ] Organisation du projet
- [ ] Techno 
- [ ] Dépendances 
- [ ] Commentaires
- [ ] Performances
- [ ] Complexité du code 

